import functools

# Using a cached factorial function for the buying_candy_new function for better performance.


@functools.lru_cache
def factorial(n: int) -> int:
    if n < 0:
        print("input has to be a positive integer")
        return False
    if (n == 0):
        return 1
    return n*factorial(n-1)

# The resulting series f(n) is a Fibonacci series so we can simply use that as well.


def buying_candy_new(n) -> int:

    # Getting the integer of the amount since we can only buy in integers.
    # making sure a float value is converted to floor(n)
    # since no candy could be bought with the amount above floor(n)
    try:
        n = int(n)
    except:
        print("Input has to be an int or float")
        return False

    if (n <= 0):
        return 0

    elif n < 2:
        return 1

    combinations = 0  # all 1s

    # loop over all possible occurences of 2 in a combination.
    limit = n//2 + 1

    for num_of_2 in range(0, limit):

        # number of 1s will always be n - 2*num_of_2
        num_of_1 = int(n - 2*num_of_2)

        # This formula calculates all the unique combinations of 1s and 2s.
        # It'll exclude combinations which only differ in the order of the same number.
        # for example: if (1_1, 1_2, 2_1, 2_2) is included, (1_1, 1_2, 2_2, 2_1) will be excluded.
        # (number of 1s + number of 2s)! / ( (number of 1)! * (number of 2)! )
        # A cached factorial is used here to improve performance.
        combinations += factorial(num_of_1 + num_of_2) // \
            (factorial(num_of_1) * factorial(num_of_2))

    return combinations

#####################################################################################################################

# If however, the question was asking for unique combinations only, where (1,1,2) is the same as (1,2,1) for example,
# the function below will work. This one does not reproduce the example give in the question though.
# buying_candy(4) != 5. I wrote this one for completeness.

#####################################################################################################################


def buying_candy(amount_of_money):

    # Getting the integer of the amount since we can only buy in integers.
    # making sure a float value is converted to floor(n)
    # since no candy could be bought with the amount above floor(n)
    try:
        amount_of_money = int(amount_of_money)
    except:
        print("Input has to be an int or float")
        return False

    if (amount_of_money <= 0):
        return 0

    elif amount_of_money < 2:
        return 1

    # Initializing the dictionary with every amount_of_money value below the largest coin
    # denomination which is 2 in our case since there is only one way to get these smaller
    # amounts (by all 1s).

    dp = {
        0: 1,
        1: 1
    }

    coin = 2
    x = coin

    while x <= amount_of_money:

        if (x not in dp):

            # Same logic as above. At every x value there's always the
            # combination of 1s that will be a combination which means we have one
            # from there already
            dp[x] = 1

        # Instead of [x-1] below it should be [x-2] where 2 is the coin we're working with
        # We can generalize this function for any number of coin denominations by simply
        # looping over a coin array.
        dp[x] = dp[x] + dp[x - coin]

        x += 1

    combinations = dp[amount_of_money]

    return combinations


if __name__ == '__main__':
    print(buying_candy_new(4))
    print(buying_candy(4))
