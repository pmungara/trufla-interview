// Removed 'export' because its just a script and not part of a module.

function stack(stackOperation, stackValue) {
  // If we want stackHolder to be accessible from outside the stack function, we should define it outside the scope
  // of this function. Otherwise every operation will be done on this stackHolder.storage
  // For example if we do two pop operations in a row, it'll return the same element since its only being updated in the local
  // scope and with every call to the function stack, stackHolder will return back to the original.

  var stackHolder = {
    // Assuming this count property to be the index of the top element in the stack and not the size of the stack.
    // In any case its not strictly required since we can just use stackHolder.storage.length for the length or
    // stackHolder.storage.length - 1 for the top element's index.
    count: 3,
    storage: [1, '{id: 1,value: "obj"}', "stringHolder", 46],
  };

  var push = function (value) {
    stackHolder.storage[stackHolder.count + 1] = value;

    // Could also do a regular js push operation.
    // stackHolder.storage.push(value);

    stackHolder.count++;
    return stackHolder.storage;
  };

  var pop = function () {
    if (stackHolder.count < 0) {
      return [];
    }

    // var poppedItem = stackHolder.storage[stackHolder.count];
    // delete stackHolder.storage[stackHolder.count];

    // I'd rather do a pop operation (since it returns the element and removes it automatically) instead of deleting,
    // because deleting an element from an array leaves a hole. Alternatively, we can also filter out the array using array.filter()

    var poppedItem = stackHolder.storage.pop();
    stackHolder.count--;

    return poppedItem;
  };

  var peek = function () {
    if (stackHolder.count >= 0) {
      // Changed the array index required from 0 to stackHolder.count
      return stackHolder.storage[stackHolder.count];
    } else {
      return stackHolder.storage;
    }
  };

  var swap = function () {
    if (stackHolder.count >= 2) {
      const count = stackHolder.count;
      const temp = stackHolder.storage[count];
      stackHolder.storage[count] = stackHolder.storage[count - 1];
      stackHolder.storage[count - 1] = temp;

      return stackHolder.storage;
    }
    console.log("Not enough elements in the stack for a swap");
    return stackHolder.storage;
  };

  switch (stackOperation) {
    case "push":
      return push(stackValue);
      break;
    case "pop":
      return pop();
      break;
    case "swap":
      return swap();
      break;
    case "peek":
      return peek();
      break;
    default:
      return stackHolder.storage;
  }
}

const updatedStackAfterPush = stack(
  (stackOperation = "push"),
  (stackValue = "Trufla")
);
console.log({ updatedStackAfterPush });

const updatedStackAfterPop = stack((stackOperation = "pop"));
console.log({ "Popped Item": updatedStackAfterPop });

const currentTop = stack((stackOperation = "peek"));
console.log({ currentTop });

const updatedStackAfterSwap = stack((stackOperation = "swap"));
console.log({ updatedStackAfterSwap });

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// // An implementation of a stack using ES6 classes.

// class stack {
//     constructor(arr) {
//         this.arr = arr;
//     }

//     push(value) {
//         this.arr.push(value);
//         return this.arr;
//     }

//     pop() {
//         // if the array is empty, return an empty array
//         if (this.arr.length === 0) {
//             return [];
//         }
//         const poppedItem = this.arr.pop();
//         return poppedItem;
//     }

//     peek() {
//           if (this.arr.length > 0) {
//             return this.arr[this.arr.length - 1];
//           }
//           console.log("Stack is empty");
//           return this.arr;
//     }

//     swap() {
//       if (this.arr.length >= 2) {
//         const top = this.arr.length - 1 // top in the stack
//         const temp = this.arr[top];
//         this.arr[top] = this.arr[top - 1];
//         this.arr[top-1] = temp;

//         return this.arr;
//       }

//       console.log("Not enough elements in the stack");
//       return this.arr;
//     }

//     // Just returns the array.
//     show() {
//         return this.arr;
//     }

// }

// const main = () => {

//     const someStack = new stack([0,1,2,3,4,5]);

//     someStack.push(6); // [0,1,2,3,4,5,6]
//     console.log("After pushing", someStack.show());
//     someStack.pop(); // 6, [0,1,2,3,4,5]
//     console.log("After popping", someStack.show());
//     someStack.peek(); // 5
//     console.log("After peeking", someStack.show());
//     someStack.swap(); // [0,1,2,3,5,4]
//     console.log("After swapping", someStack.show());

// }

// main();
