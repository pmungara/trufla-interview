SELECT 
  c.name, 
  COUNT(final.eid) 
FROM 
  country AS c 
  -- Left joining to include countries that dont have any employees
  LEFT JOIN (
    SELECT 
      *, 
      -- If one person is working at multiple locations, take the location where they have been working the longest.
      MIN(start_date)
    FROM 
      (
        SELECT 
          ewc.employee_id AS eid, 
          ewc.start_date AS start_date, 
          e.first_name AS efname, 
          e.country_id AS ecid, 
          wc.country_id AS wccid 
        FROM 
          employee_working_center AS ewc
          -- Only get employees who currently work here
          LEFT JOIN employee AS e ON e.id = ewc.employee_id 
          JOIN working_center AS wc ON wc.id = ewc.working_center_id 
        WHERE 
          -- Get employees who have worked for at least one year
          start_date <= DATE('now', '-1 years')
        
        -- Ordering ascending to make sure the country ids are ordered min-max if start dates are the same for a given employee.
        order by IFNULL(ecid, wccid) ASC
      ) 
    GROUP BY 
      eid
  ) AS final ON IFNULL(ecid, wccid) = c.id
-- Group by country names
GROUP BY 
  c.name
-- Sort by counts in descending order but if two countries have the same count, sort them in ascending order.
ORDER BY 
  COUNT(final.eid) DESC, 
  c.name ASC
